package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.BookerStoreService;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BookServiceTest {

    @Mock
    private BookerStoreService bookerStoreService;

    private BookService bookService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        bookService = new BookService(bookerStoreService);
    }

    @Test
    public void testIsDataAlreadyImported() {
        when(bookerStoreService.isDataAlreadyImported()).thenReturn(true);
        bookService.isDataAlreadyImported();
        verify(bookerStoreService).isDataAlreadyImported();
    }
}
