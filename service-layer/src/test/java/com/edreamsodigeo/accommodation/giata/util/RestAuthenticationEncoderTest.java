package com.edreamsodigeo.accommodation.giata.util;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RestAuthenticationEncoderTest {

    @Test
    public void testCreateAuthorization() {
        // given
        String username = "username";
        String password = "password";

        //when
        String result = RestAuthenticationEncoder.createAuthorization(username, password);

        //then
        Assert.assertEquals(result, "Basic dXNlcm5hbWU6cGFzc3dvcmQ=");
    }

}
