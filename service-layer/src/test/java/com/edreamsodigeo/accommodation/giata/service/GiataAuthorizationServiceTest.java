package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.GiataMulticodesServiceConfiguration;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.mockito.Mockito.when;

public class GiataAuthorizationServiceTest {

    @Mock
    private GiataMulticodesServiceConfiguration configuration;

    GiataAuthorizationService giataAuthorizationService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        giataAuthorizationService = new GiataAuthorizationService(configuration);
    }

    @Test
    public void testCreateAuthorization() {
        String username = "username";
        String password = "password";

        when(configuration.getUsername()).thenReturn(username);
        when(configuration.getPassword()).thenReturn(password);

        String authorization = giataAuthorizationService.createGiataAuthorization();
        assertAuthorization(authorization, username, password);
    }

    private void assertAuthorization(String authorization, String username, String password) {
        Assert.assertTrue(authorization.contains("Basic "));
        String encodedUsrAndPass = authorization.replace("Basic ", "");
        String usrAndPass = new String(Base64.getDecoder().decode(encodedUsrAndPass), StandardCharsets.UTF_8);
        Assert.assertTrue(usrAndPass.contains(username));
        Assert.assertTrue(usrAndPass.contains(password));
    }
}
