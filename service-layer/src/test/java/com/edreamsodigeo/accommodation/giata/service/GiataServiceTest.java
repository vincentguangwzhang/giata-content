package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.GiataMulticodesStoreService;
import com.edreamsodigeo.accommodation.giata.dto.Properties;
import com.edreamsodigeo.accommodation.giata.endpoints.GiataMulticodesService;
import com.edreamsodigeo.accommodation.giata.exceptions.GDSNotPermitException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GiataServiceTest {

    @Mock
    private GiataMulticodesService giataDataService;

    @Mock
    private GiataMulticodesStoreService giataMulticodesStoreService;

    @Mock
    private GiataAuthorizationService giataAuthorizationService;

    private GiataService giataService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        giataService = new GiataService(giataDataService, giataMulticodesStoreService, giataAuthorizationService);
    }

    @Test(expectedExceptions = GDSNotPermitException.class)
    public void testWhenGDSIsNotValid() {
        String gds = "not_valid_gds";
        giataService.loadGiataData(gds);
    }

    @Test
    public void testWhenDetailPropertiesHasNoMoreLink() {
        Properties properties = new Properties();
        properties.setMore(null);
        String auth = "authorization";
        String gds = "hotelbeds";

        when(giataDataService.getPropertiesByGDS(auth, gds)).thenReturn(properties);
        when(giataAuthorizationService.createGiataAuthorization()).thenReturn(auth);
        doNothing().when(giataMulticodesStoreService).deleteGiataMulticodesByGdsCode(gds);

        giataService.loadGiataData(gds);

        verify(giataDataService).getPropertiesByGDS(auth, gds);
        verify(giataDataService, never()).getPropertiesByGDSAndOffset(any(), any(), any());
        verify(giataMulticodesStoreService).deleteGiataMulticodesByGdsCode(gds);
    }

    @Test
    public void testWhenDetailPropertiesHasMoreLink() {
        String url = "https://multicodes.giatamedia.com/webservice/rest/1.0/properties/multi/gds/hotelbeds/offset/1285264";
        Properties properties = new Properties();
        Properties.More more = new Properties.More();
        more.setHref(url);
        properties.setMore(more);

        Properties secondProperties = new Properties();
        secondProperties.setMore(null);
        String auth = "authorization";
        String gds = "hotelbeds";

        when(giataDataService.getPropertiesByGDS(auth, gds)).thenReturn(properties);
        when(giataDataService.getPropertiesByGDSAndOffset(auth, gds, "1285264")).thenReturn(secondProperties);
        when(giataAuthorizationService.createGiataAuthorization()).thenReturn(auth);
        doNothing().when(giataMulticodesStoreService).deleteGiataMulticodesByGdsCode(gds);

        giataService.loadGiataData(gds);

        verify(giataDataService, times(1)).getPropertiesByGDS(auth, gds);
        verify(giataDataService, times(1)).getPropertiesByGDSAndOffset(auth, gds, "1285264");
        verify(giataMulticodesStoreService).deleteGiataMulticodesByGdsCode(gds);
    }
}
