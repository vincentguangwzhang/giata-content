package com.edreamsodigeo.accommodation.giata;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.repository.ProspectiveRepository;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProspectiveStoreServiceBeanTest {

    @Mock
    private ProspectiveRepository prospectiveRepository;

    private ProspectiveStoreServiceBean prospectiveStoreServiceBean;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ConfigurationEngine.init(binder -> binder.bind(ProspectiveRepository.class).toInstance(prospectiveRepository));
        this.prospectiveStoreServiceBean = ConfigurationEngine.getInstance(ProspectiveStoreServiceBean.class);
    }

    @Test
    public void testSaveProspectiveDTOList() {
        // given
        List<ProspectiveDTO> prospectiveDTOList = new ArrayList<>();
        doNothing().when(prospectiveRepository).saveProspectiveDTOListInBatch(prospectiveDTOList);

        // when
        prospectiveStoreServiceBean.saveProspectiveDTOList(prospectiveDTOList);

        // then
        verify(prospectiveRepository).saveProspectiveDTOListInBatch(prospectiveDTOList);
    }

    @Test
    public void testIsDataAlreadyImported() {
        when(prospectiveRepository.isDataAlreadyImported()).thenReturn(true);

        prospectiveStoreServiceBean.isDataAlreadyImported();

        verify(prospectiveRepository).isDataAlreadyImported();
    }

}
