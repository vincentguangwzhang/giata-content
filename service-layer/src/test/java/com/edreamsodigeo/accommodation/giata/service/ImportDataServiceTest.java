package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.exceptions.EDODataAlreadyImportException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ImportDataServiceTest {

    @Mock
    private ProspectiveService prospectiveService;

    @Mock
    private BookService bookService;

    private ImportDataService importDataService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        importDataService = new ImportDataService(prospectiveService, bookService);
    }

    @Test(expectedExceptions = EDODataAlreadyImportException.class)
    public void testWhenBookerDataAlreadyImport() {
        when(bookService.isDataAlreadyImported()).thenReturn(true);
        when(prospectiveService.isDataAlreadyImported()).thenReturn(false);
        doNothing().when(bookService).loadBookFileAndSave();
        doNothing().when(prospectiveService).loadProspectiveFileAndSave();

        importDataService.importEDOData();

        verify(bookService).isDataAlreadyImported();
        verify(bookService, never()).loadBookFileAndSave();
        verify(prospectiveService).isDataAlreadyImported();
        verify(prospectiveService, never()).loadProspectiveFileAndSave();
    }

    @Test(expectedExceptions = EDODataAlreadyImportException.class)
    public void testWhenProspectiveDataAlreadyImport() {
        when(bookService.isDataAlreadyImported()).thenReturn(false);
        when(prospectiveService.isDataAlreadyImported()).thenReturn(true);
        doNothing().when(bookService).loadBookFileAndSave();
        doNothing().when(prospectiveService).loadProspectiveFileAndSave();

        importDataService.importEDOData();

        verify(bookService).isDataAlreadyImported();
        verify(bookService, never()).loadBookFileAndSave();
        verify(prospectiveService).isDataAlreadyImported();
        verify(prospectiveService, never()).loadProspectiveFileAndSave();
    }

    @Test
    public void testImportEDOData() {
        when(bookService.isDataAlreadyImported()).thenReturn(false);
        when(prospectiveService.isDataAlreadyImported()).thenReturn(false);
        doNothing().when(bookService).loadBookFileAndSave();
        doNothing().when(prospectiveService).loadProspectiveFileAndSave();

        importDataService.importEDOData();

        verify(bookService).isDataAlreadyImported();
        verify(bookService).loadBookFileAndSave();
        verify(prospectiveService).isDataAlreadyImported();
        verify(prospectiveService).loadProspectiveFileAndSave();
    }
}
