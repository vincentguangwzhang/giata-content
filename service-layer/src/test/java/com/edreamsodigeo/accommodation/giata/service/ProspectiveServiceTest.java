package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.ProspectiveStoreService;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProspectiveServiceTest {

    @Mock
    private ProspectiveStoreService prospectiveStoreService;

    private ProspectiveService prospectiveService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        prospectiveService = new ProspectiveService(prospectiveStoreService);
    }

    @Test
    public void testIsDataAlreadyImported() {
        when(prospectiveStoreService.isDataAlreadyImported()).thenReturn(true);
        prospectiveService.isDataAlreadyImported();
        verify(prospectiveStoreService).isDataAlreadyImported();
    }
}
