package com.edreamsodigeo.accommodation.giata;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.repository.GiataMulticodesRepository;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

public class GiataMulticodesStoreServiceBeanTest {

    private GiataMulticodesStoreServiceBean giataMulticodesStoreServiceBean;

    @Mock
    private GiataMulticodesRepository giataMulticodesRepository;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ConfigurationEngine.init(binder -> binder.bind(GiataMulticodesRepository.class).toInstance(giataMulticodesRepository));
        this.giataMulticodesStoreServiceBean = ConfigurationEngine.getInstance(GiataMulticodesStoreServiceBean.class);
    }

    @Test
    public void testCreateGiataMulticodes() {
        List<GiataMulticodesDTO> giataMulticodesDTOList = new ArrayList<>();

        doNothing().when(giataMulticodesRepository).createGiataMulticodesInBatch(giataMulticodesDTOList);

        giataMulticodesStoreServiceBean.createGiataMulticodes(giataMulticodesDTOList);

        verify(giataMulticodesRepository).createGiataMulticodesInBatch(giataMulticodesDTOList);
    }

    @Test
    public void testDeleteGiataMulticodesByGdsCode() {
        String gds = "a gds code";
        doNothing().when(giataMulticodesRepository).deleteGiataMulticodesByGds(gds);

        giataMulticodesStoreServiceBean.deleteGiataMulticodesByGdsCode(gds);

        verify(giataMulticodesRepository).deleteGiataMulticodesByGds(gds);
    }

}
