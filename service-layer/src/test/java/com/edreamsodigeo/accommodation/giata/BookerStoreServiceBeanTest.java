package com.edreamsodigeo.accommodation.giata;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.repository.BookerRepository;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BookerStoreServiceBeanTest {

    @Mock
    private BookerRepository bookerRepository;

    private BookerStoreServiceBean bookerStoreServiceBean;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ConfigurationEngine.init(binder -> binder.bind(BookerRepository.class).toInstance(bookerRepository));
        this.bookerStoreServiceBean = ConfigurationEngine.getInstance(BookerStoreServiceBean.class);
    }

    @Test
    public void testSaveBookerDTOList() {
        //given
        List<BookerDTO> bookerDTOList = new ArrayList<>();
        doNothing().when(bookerRepository).saveBookerDTOListInBatch(bookerDTOList);

        //when
        bookerStoreServiceBean.saveBookerDTOList(bookerDTOList);

        //then
        verify(bookerRepository, times(1)).saveBookerDTOListInBatch(bookerDTOList);
    }

    @Test
    public void testIsDataAlreadyImported() {
        when(bookerRepository.isDataAlreadyImported()).thenReturn(true);
        bookerStoreServiceBean.isDataAlreadyImported();
        verify(bookerRepository).isDataAlreadyImported();
    }

}
