package com.edreamsodigeo.accommodation.giata.service;

import com.edreams.util.file.FileUtilities;
import com.edreamsodigeo.accommodation.giata.ProspectiveDTO;
import com.edreamsodigeo.accommodation.giata.ProspectiveStoreService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class ProspectiveService {

    private static final Logger LOGGER = Logger.getLogger(ProspectiveService.class);

    private final ProspectiveStoreService prospectiveStoreService;

    private static final String CSV_PATH = "/com/edreamsodigeo/accommodation/giata/service/prospective.csv";

    @Inject
    public ProspectiveService(final ProspectiveStoreService prospectiveStoreService) {
        this.prospectiveStoreService = prospectiveStoreService;
    }

    public void loadProspectiveFileAndSave() {
        String strContent = FileUtilities.readFile(CSV_PATH, ProspectiveService.class);
        List<ProspectiveDTO> prospectiveDTOList = new ArrayList<>();

        String[] contentArray = strContent.split("\\n");
        for (String line : contentArray) {
            try {
                String[] temp = line.split(",");
                prospectiveDTOList.add(
                        new ProspectiveDTO(
                                Long.parseLong(temp[0]),
                                BigDecimal.valueOf(Double.valueOf(temp[1]))
                        )
                );
            } catch (Exception e) {
                LOGGER.warn("Format error, line = " + line, e);
            }
        }

        prospectiveStoreService.saveProspectiveDTOList(prospectiveDTOList);
    }

    public boolean isDataAlreadyImported() {
        return prospectiveStoreService.isDataAlreadyImported();
    }

}
