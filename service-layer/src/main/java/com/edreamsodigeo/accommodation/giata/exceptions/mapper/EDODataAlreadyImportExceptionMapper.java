package com.edreamsodigeo.accommodation.giata.exceptions.mapper;

import com.edreamsodigeo.accommodation.giata.exceptions.EDODataAlreadyImportException;
import com.odigeo.commons.rest.error.DefaultExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EDODataAlreadyImportExceptionMapper extends DefaultExceptionMapper<EDODataAlreadyImportException> implements ExceptionMapper<EDODataAlreadyImportException> {

    @Override
    public boolean sendExceptionCause() {
        return true;
    }

    @Override
    public Response.Status statusToSend() {
        return Response.Status.CONFLICT;
    }

}
