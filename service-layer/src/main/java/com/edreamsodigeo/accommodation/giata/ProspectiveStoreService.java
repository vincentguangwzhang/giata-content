package com.edreamsodigeo.accommodation.giata;

import java.util.List;

public interface ProspectiveStoreService {

    void saveProspectiveDTOList(final List<ProspectiveDTO> prospectiveDTOList);

    boolean isDataAlreadyImported();

}
