package com.edreamsodigeo.accommodation.giata;

import java.util.List;

public interface BookerStoreService {

    void saveBookerDTOList(final List<BookerDTO> bookerDTOList);

    boolean isDataAlreadyImported();

}
