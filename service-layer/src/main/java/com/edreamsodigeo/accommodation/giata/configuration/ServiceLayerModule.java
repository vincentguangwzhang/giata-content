package com.edreamsodigeo.accommodation.giata.configuration;

import com.edreamsodigeo.accommodation.giata.BookerStoreService;
import com.edreamsodigeo.accommodation.giata.GiataMulticodesStoreService;
import com.edreamsodigeo.accommodation.giata.ProspectiveStoreService;
import com.google.inject.AbstractModule;
import com.odigeo.configuration.ServiceLocator;
import com.odigeo.configuration.UnavailableServiceException;
import org.apache.log4j.Logger;

public class ServiceLayerModule extends AbstractModule {

    private static final Logger LOGGER = Logger.getLogger(ServiceLayerModule.class);

    @Override
    public void configure() {
        bind(ServiceLocator.class).toInstance(JeeServiceLocator.getInstance());
        configureJeeServices();
    }

    private void configureJeeServices() {
        configureJeeService(GiataMulticodesStoreService.class);
        configureJeeService(ProspectiveStoreService.class);
        configureJeeService(BookerStoreService.class);
        LOGGER.info("All services have been configured");
    }

    private <T> void configureJeeService(Class<T> clazz) {
        configureJeeService(clazz, clazz);
    }

    private <T> void configureJeeService(Class<T> clazz, Class<? extends T> providerClazz) {
        try {
            bind(clazz).toProvider(JeeServiceProvider.getInstance(providerClazz));
        } catch (UnavailableServiceException usex) {
            LOGGER.error("Unable to create provider " + providerClazz + " for class " + clazz, usex);
        }
    }


}
