package com.edreamsodigeo.accommodation.giata.service;

import com.edreams.util.file.FileUtilities;
import com.edreamsodigeo.accommodation.giata.BookerDTO;
import com.edreamsodigeo.accommodation.giata.BookerStoreService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@Singleton
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class BookService {

    private static final Logger LOGGER = Logger.getLogger(BookService.class);

    private final BookerStoreService bookerStoreService;

    private static final String CSV_PATH = "/com/edreamsodigeo/accommodation/giata/service/book.csv";

    @Inject
    public BookService(final BookerStoreService bookerStoreService) {
        this.bookerStoreService = bookerStoreService;
    }

    public void loadBookFileAndSave() {
        String strContent = FileUtilities.readFile(CSV_PATH, BookService.class);
        List<BookerDTO> bookerDTOList = new ArrayList<>();

        String[] contentArray = strContent.split("\\n");
        for (String line : contentArray) {
            try {
                String[] temp = line.split(",");
                bookerDTOList.add(
                        new BookerDTO(Long.parseLong(temp[0]))
                );
            } catch (Exception e) {
                LOGGER.warn("Format error, line = " + line, e);
            }
        }

        bookerStoreService.saveBookerDTOList(bookerDTOList);
    }

    public boolean isDataAlreadyImported() {
        return bookerStoreService.isDataAlreadyImported();
    }

}
