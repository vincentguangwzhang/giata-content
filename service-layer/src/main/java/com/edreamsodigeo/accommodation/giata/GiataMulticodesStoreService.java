package com.edreamsodigeo.accommodation.giata;

import java.util.List;

public interface GiataMulticodesStoreService {

    void createGiataMulticodes(List<GiataMulticodesDTO> giataMulticodesDTOList);

    void deleteGiataMulticodesByGdsCode(String gdsCode);

}
