package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.GiataMulticodesServiceConfiguration;
import com.edreamsodigeo.accommodation.giata.util.RestAuthenticationEncoder;
import com.google.inject.Inject;

public class GiataAuthorizationService {
    private final GiataMulticodesServiceConfiguration configuration;

    @Inject
    public GiataAuthorizationService(GiataMulticodesServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public String createGiataAuthorization() {
        return RestAuthenticationEncoder.createAuthorization(configuration.getUsername(), configuration.getPassword());
    }
}
