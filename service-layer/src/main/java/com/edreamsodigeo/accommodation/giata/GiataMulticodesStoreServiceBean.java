package com.edreamsodigeo.accommodation.giata;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.repository.GiataMulticodesRepository;
import com.edreamsodigeo.accommodation.giata.repository.impl.JpaGiataMulticodesRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@Local(GiataMulticodesStoreService.class)
public class GiataMulticodesStoreServiceBean implements GiataMulticodesStoreService {

    private final GiataMulticodesRepository giataMulticodesRepository;

    @PersistenceContext(unitName = "giata")
    private EntityManager entityManager;

    public GiataMulticodesStoreServiceBean() {
        giataMulticodesRepository = ConfigurationEngine.getInstance(GiataMulticodesRepository.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaGiataMulticodesRepository) giataMulticodesRepository).setEntityManager(entityManager);
    }

    @Override
    public void createGiataMulticodes(List<GiataMulticodesDTO> giataMulticodesDTOList) {
        giataMulticodesRepository.createGiataMulticodesInBatch(giataMulticodesDTOList);
    }

    @Override
    public void deleteGiataMulticodesByGdsCode(String gdsCode) {
        giataMulticodesRepository.deleteGiataMulticodesByGds(gdsCode);
    }

}
