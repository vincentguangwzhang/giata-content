package com.edreamsodigeo.accommodation.giata;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.repository.ProspectiveRepository;
import com.edreamsodigeo.accommodation.giata.repository.impl.JpaProspectiveRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@Local(ProspectiveStoreService.class)
public class ProspectiveStoreServiceBean implements ProspectiveStoreService {

    private final ProspectiveRepository prospectiveRepository;

    @PersistenceContext(unitName = "giata")
    private EntityManager entityManager;

    public ProspectiveStoreServiceBean() {
        prospectiveRepository = ConfigurationEngine.getInstance(ProspectiveRepository.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaProspectiveRepository) prospectiveRepository).setEntityManager(entityManager);
    }

    @Override
    public void saveProspectiveDTOList(List<ProspectiveDTO> prospectiveDTOList) {
        prospectiveRepository.saveProspectiveDTOListInBatch(prospectiveDTOList);
    }

    @Override
    public boolean isDataAlreadyImported() {
        return prospectiveRepository.isDataAlreadyImported();
    }
}
