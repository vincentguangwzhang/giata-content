package com.edreamsodigeo.accommodation.giata.util;

import java.nio.charset.Charset;
import java.util.Base64;

public final class RestAuthenticationEncoder {

    private RestAuthenticationEncoder() {

    }

    public static String createAuthorization(final String username, final String password) {
        return "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes(Charset.forName("UTF-8")));
    }
}
