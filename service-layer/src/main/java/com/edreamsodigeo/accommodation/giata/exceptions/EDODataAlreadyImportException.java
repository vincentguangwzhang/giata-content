package com.edreamsodigeo.accommodation.giata.exceptions;

public class EDODataAlreadyImportException extends RuntimeException {

    private static final long serialVersionUID = 3173339606520011136L;

    public static final String MSG_DATA_ALREADY_IMPORT = "EDO data already imported to Prospective or Book table, please clear first";

    public EDODataAlreadyImportException(String message, Throwable cause) {
        super(message, cause);
    }

    public EDODataAlreadyImportException() {
        super(MSG_DATA_ALREADY_IMPORT);
    }
}
