package com.edreamsodigeo.accommodation.giata.monitoring;

import com.edreamsodigeo.accommodation.giata.util.RestAuthenticationEncoder;
import com.odigeo.commons.monitoring.healthcheck.HttpHealthCheck;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class GiataMulticodesServiceHttpHealthCheck extends HttpHealthCheck {

    private static final Logger LOGGER = Logger.getLogger(GiataMulticodesServiceHttpHealthCheck.class);
    private static final String USERNAME = "info|roamamore.com";
    private static final String PASSWORD = "Roam321!";
    private final String urlPath;

    public GiataMulticodesServiceHttpHealthCheck(String url) throws MalformedURLException {
        super(url);
        urlPath = url;
    }

    @Override
    protected Result check() {
        HttpClient client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1).build();
        HttpRequest request;
        try {
            request = HttpRequest.newBuilder()
                    .uri(new URI(urlPath))
                    .header("Authorization", basicAuth(USERNAME, PASSWORD))
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == HttpStatus.SC_OK) {
                return Result.healthy();
            }
        } catch (URISyntaxException | IOException | InterruptedException e) {
            LOGGER.warn("Exception happen when try to do health check", e);
            return Result.unhealthy(e.getMessage());
        }

        return Result.unhealthy("");
    }

    private static String basicAuth(String username, String password) {
        return RestAuthenticationEncoder.createAuthorization(username, password);
    }
}
