package com.edreamsodigeo.accommodation.giata.configuration;

public final class ServiceNameResolver {

    private static final ServiceNameResolver THE_INSTANCE = new ServiceNameResolver();

    private ServiceNameResolver() {

    }

    public String resolveServiceContext(String applicationName) {
        return "java:global/" + applicationName + '/' + "service-layer/";
    }

    public <T> String resolveServiceName(Class<T> serviceType, String serviceContext) {
        return serviceContext + serviceType.getSimpleName() + "Bean";
    }

    public static ServiceNameResolver getInstance() {
        return THE_INSTANCE;
    }
}
