package com.edreamsodigeo.accommodation.giata.exceptions;

public class GDSNotPermitException extends RuntimeException {

    private static final long serialVersionUID = 3173339606520011136L;

    public static final String MSG_GDS_NOT_VALID = "GDS not valid, it should be one of booking.com, hotelbeds, expedia_rapid";

    public GDSNotPermitException(String message, Throwable cause) {
        super(message, cause);
    }

    public GDSNotPermitException() {
        super(MSG_GDS_NOT_VALID);
    }

}
