package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.exceptions.EDODataAlreadyImportException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.log4j.Logger;

@Singleton
public class ImportDataService {

    private static final Logger LOGGER = Logger.getLogger(ImportDataService.class);

    private final ProspectiveService prospectiveService;

    private final BookService bookService;

    @Inject
    public ImportDataService(final ProspectiveService prospectiveService, final BookService bookService) {
        this.prospectiveService = prospectiveService;
        this.bookService = bookService;
    }

    public void importEDOData() {
        checkIfDataAlreadyImportedAndThrow();
        LOGGER.info("Begin to import data");

        this.prospectiveService.loadProspectiveFileAndSave();
        this.bookService.loadBookFileAndSave();
    }

    private void checkIfDataAlreadyImportedAndThrow() {
        if (this.prospectiveService.isDataAlreadyImported() || this.bookService.isDataAlreadyImported()) {
            throw new EDODataAlreadyImportException();
        }
    }

}
