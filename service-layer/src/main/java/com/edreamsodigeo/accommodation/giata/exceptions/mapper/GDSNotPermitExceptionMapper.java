package com.edreamsodigeo.accommodation.giata.exceptions.mapper;

import com.edreamsodigeo.accommodation.giata.exceptions.GDSNotPermitException;
import com.odigeo.commons.rest.error.DefaultExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GDSNotPermitExceptionMapper extends DefaultExceptionMapper<GDSNotPermitException> implements ExceptionMapper<GDSNotPermitException> {
    @Override
    public boolean sendExceptionCause() {
        return true;
    }

    @Override
    public Response.Status statusToSend() {
        return Response.Status.NOT_FOUND;
    }
}
