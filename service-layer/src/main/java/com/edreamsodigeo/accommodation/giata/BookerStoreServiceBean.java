package com.edreamsodigeo.accommodation.giata;


import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.repository.BookerRepository;
import com.edreamsodigeo.accommodation.giata.repository.impl.JpaBookerRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@Local(BookerStoreService.class)
public class BookerStoreServiceBean implements BookerStoreService {

    private final BookerRepository bookerRepository;

    @PersistenceContext(unitName = "giata")
    private EntityManager entityManager;

    public BookerStoreServiceBean() {
        bookerRepository = ConfigurationEngine.getInstance(BookerRepository.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaBookerRepository) bookerRepository).setEntityManager(entityManager);
    }

    @Override
    public void saveBookerDTOList(List<BookerDTO> bookerDTOList) {
        bookerRepository.saveBookerDTOListInBatch(bookerDTOList);
    }

    @Override
    public boolean isDataAlreadyImported() {
        return bookerRepository.isDataAlreadyImported();
    }
}
