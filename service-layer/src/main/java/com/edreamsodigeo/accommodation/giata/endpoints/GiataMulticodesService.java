package com.edreamsodigeo.accommodation.giata.endpoints;

import com.edreamsodigeo.accommodation.giata.dto.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/webservice/rest/1.0/properties")
public interface GiataMulticodesService {

    @GET
    @Path("/multi/gds/{gds}")
    Properties getPropertiesByGDS(@HeaderParam ("Authorization") String authorization, @PathParam("gds") String gds);


    @GET
    @Path("/multi/gds/{gds}/offset/{offset}")
    Properties getPropertiesByGDSAndOffset(@HeaderParam ("Authorization") String authorization, @PathParam("gds") String gds, @PathParam("offset") String offset);
}
