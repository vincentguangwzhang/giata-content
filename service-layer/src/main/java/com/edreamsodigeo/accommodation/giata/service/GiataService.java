package com.edreamsodigeo.accommodation.giata.service;

import com.edreamsodigeo.accommodation.giata.GiataMulticodesDTO;
import com.edreamsodigeo.accommodation.giata.GiataMulticodesStoreService;
import com.edreamsodigeo.accommodation.giata.dto.Properties;
import com.edreamsodigeo.accommodation.giata.dto.PropertyCodesType;
import com.edreamsodigeo.accommodation.giata.dto.PropertyDetailsType;
import com.edreamsodigeo.accommodation.giata.endpoints.GiataMulticodesService;
import com.edreamsodigeo.accommodation.giata.exceptions.GDSNotPermitException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Singleton
public class GiataService {

    private static final Logger LOGGER = Logger.getLogger(GiataService.class);

    private static final String[] VALID_GDS = {"booking.com", "hotelbeds", "expedia_rapid"};

    private final GiataMulticodesService giataMulticodesService;

    private final GiataMulticodesStoreService giataMulticodesStoreService;
    private final GiataAuthorizationService giataAuthorizationService;

    @Inject
    public GiataService(GiataMulticodesService giataMulticodesService, GiataMulticodesStoreService giataMulticodesStoreService, GiataAuthorizationService giataAuthorizationService) {
        this.giataMulticodesService = giataMulticodesService;
        this.giataMulticodesStoreService = giataMulticodesStoreService;
        this.giataAuthorizationService = giataAuthorizationService;
    }

    public void loadGiataData(final String gds) {
        verifyIfGDSValidOrThrow(gds);
        emptyExistingDataByGdsBeforeLoad(gds);

        Properties properties = giataMulticodesService.getPropertiesByGDS(giataAuthorizationService.createGiataAuthorization(), gds);
        if (Objects.nonNull(properties) && !CollectionUtils.isEmpty(properties.getProperty())) {
            List<GiataMulticodesDTO> list = extractPropertiesToGiataMurtiCodesListByGDS(properties, gds);
            if (!CollectionUtils.isEmpty(list)) {
                this.giataMulticodesStoreService.createGiataMulticodes(list);
            }
        }

        while (Objects.nonNull(properties) && Objects.nonNull(properties.getMore()) && StringUtils.isNotEmpty(properties.getMore().getHref())) {
            String nextURL = properties.getMore().getHref();
            LOGGER.info("Begin next page: " + nextURL);
            String offset = nextURL.substring(nextURL.lastIndexOf('/') + 1);
            properties = giataMulticodesService.getPropertiesByGDSAndOffset(giataAuthorizationService.createGiataAuthorization(), gds, offset);
            if (Objects.nonNull(properties) && !CollectionUtils.isEmpty(properties.getProperty())) {
                List<GiataMulticodesDTO> list = extractPropertiesToGiataMurtiCodesListByGDS(properties, gds);
                if (!CollectionUtils.isEmpty(list)) {
                    this.giataMulticodesStoreService.createGiataMulticodes(list);
                }
            }
        }
    }

    private void emptyExistingDataByGdsBeforeLoad(String gds) {
        this.giataMulticodesStoreService.deleteGiataMulticodesByGdsCode(gds);
    }

    private void verifyIfGDSValidOrThrow(final String gds) {
        if (!Arrays.asList(VALID_GDS).contains(gds)) {
            throw new GDSNotPermitException();
        }
    }

    private List<GiataMulticodesDTO> extractPropertiesToGiataMurtiCodesListByGDS(final Properties properties, final String gds) {
        List<GiataMulticodesDTO> results = new ArrayList<>();

        List<PropertyDetailsType> propertyDetailsTypes = properties.getProperty();
        for (PropertyDetailsType propertyDetailsType : propertyDetailsTypes) {
            Long giataId = (long) propertyDetailsType.getGiataId();

            PropertyCodesType propertyCodesType = propertyDetailsType.getPropertyCodes();
            List<PropertyCodesType.Provider> providers = propertyCodesType.getProvider();
            Optional<PropertyCodesType.Provider> optionalProvider = providers.stream().filter(p -> p.getProviderCode().equalsIgnoreCase(gds)).findFirst();
            if (optionalProvider.isPresent()) {
                PropertyCodesType.Provider provider = optionalProvider.get();
                List<PropertyCodesType.Provider.Code> codes = provider.getCode();
                for (PropertyCodesType.Provider.Code code : codes) {
                    results.add(new GiataMulticodesDTO(giataId, gds, code.getValue().get(0).getValue()));
                }
            }
        }
        return results;
    }
}
