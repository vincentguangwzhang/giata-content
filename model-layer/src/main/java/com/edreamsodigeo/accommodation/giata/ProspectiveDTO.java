package com.edreamsodigeo.accommodation.giata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProspectiveDTO {
    private Long dedupId;
    private BigDecimal searchesPerDay;
}
