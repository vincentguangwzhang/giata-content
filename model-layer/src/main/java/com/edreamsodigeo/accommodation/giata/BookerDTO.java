package com.edreamsodigeo.accommodation.giata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookerDTO {
    private Long expediaId;
}
