package com.edreamsodigeo.accommodation.service;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.service.GiataService;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

public class GiataControllerTest {

    @Mock
    private GiataService giataService;

    private GiataController giataController;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ConfigurationEngine.init(binder -> binder.bind(GiataService.class).toInstance(giataService));
        this.giataController = ConfigurationEngine.getInstance(GiataController.class);
    }

    @Test
    public void testLoadGiataData() {
        String gds = "a_gds";
        doNothing().when(giataService).loadGiataData(gds);
        giataController.loadDataByGDS(gds);
        verify(giataService).loadGiataData(gds);
    }

}
