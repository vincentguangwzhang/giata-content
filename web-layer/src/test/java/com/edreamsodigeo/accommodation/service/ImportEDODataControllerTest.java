package com.edreamsodigeo.accommodation.service;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.accommodation.giata.service.ImportDataService;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

public class ImportEDODataControllerTest {

    @Mock
    private ImportDataService importDataService;

    private ImportEDODataController importEDODataController;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ConfigurationEngine.init(binder -> binder.bind(ImportDataService.class).toInstance(importDataService));
        this.importEDODataController = ConfigurationEngine.getInstance(ImportEDODataController.class);
    }

    @Test
    public void testImportData() {
        doNothing().when(importDataService).importEDOData();
        importEDODataController.importData();
        verify(importDataService).importEDOData();
    }
}
