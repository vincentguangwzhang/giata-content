package com.edreamsodigeo.accommodation.service;

import com.edreamsodigeo.accommodation.giata.service.ImportDataService;
import com.google.inject.Inject;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/files")
public class ImportEDODataController {

    private final ImportDataService importDataService;

    @Inject
    public ImportEDODataController(final ImportDataService importDataService) {
        this.importDataService = importDataService;
    }

    @POST
    @Path("import")
    public void importData() {
        this.importDataService.importEDOData();
    }

}
