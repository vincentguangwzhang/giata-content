package com.edreamsodigeo.accommodation.service;

import com.edreamsodigeo.accommodation.giata.service.GiataService;
import com.google.inject.Inject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/giata")
public class GiataController {

    private final GiataService giataService;

    @Inject
    public GiataController(final GiataService giataService) {
        this.giataService = giataService;
    }

    @GET
    @Path("gds/{gds}")
    @Produces(MediaType.APPLICATION_JSON)
    public void loadDataByGDS(@PathParam("gds") String gds) {
        giataService.loadGiataData(gds);
    }

}
