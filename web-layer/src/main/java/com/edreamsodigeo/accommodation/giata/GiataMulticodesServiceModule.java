package com.edreamsodigeo.accommodation.giata;

import com.edreamsodigeo.accommodation.giata.endpoints.GiataMulticodesService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.error.JsonIgnorePropertiesContextResolver;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.commons.rest.guice.DefaultRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class GiataMulticodesServiceModule extends DefaultRestUtilsModule<GiataMulticodesService> {

    public GiataMulticodesServiceModule(ServiceNotificator... notificators) {
        super(GiataMulticodesService.class, notificators);
    }

    @Override
    protected ServiceConfiguration<GiataMulticodesService> getServiceConfiguration(Class<GiataMulticodesService> serviceContractClass) {
        ConnectionConfiguration connectionConfiguration = (new ConnectionConfiguration.Builder()).connectionTimeoutInMillis(Integer.valueOf(60000)).socketTimeoutInMillis(Integer.valueOf(60000)).maxConcurrentConnections(Integer.valueOf(500)).build();
        ServiceConfiguration<GiataMulticodesService> serviceConfiguration = (new ServiceConfiguration.Builder<GiataMulticodesService>(GiataMulticodesService.class))
                .withConnectionConfiguration(connectionConfiguration)
                .withRestErrorsHandler(new SimpleRestErrorsHandler(GiataMulticodesService.class))
                .build();
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());
        return serviceConfiguration;
    }
}
