package com.edreamsodigeo.accommodation.giata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "PROSPECTIVE")
@Access(AccessType.FIELD)
@Getter
@Setter
@NamedQueries({@NamedQuery(name = "Prospective.count", query = "select count(p) from Prospective p")})
public class Prospective {

    @Id
    @Column(name = "DEDUP_ID", nullable = false)
    private Long dedupId;

    @Column(name = "SEARCHES_PER_DAY", nullable = false, precision = 20, scale = 5)
    private BigDecimal searchesPerDay;

}
