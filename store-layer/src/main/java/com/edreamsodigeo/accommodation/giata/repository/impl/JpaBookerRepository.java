package com.edreamsodigeo.accommodation.giata.repository.impl;

import com.edreamsodigeo.accommodation.giata.BookerDTO;
import com.edreamsodigeo.accommodation.giata.mapper.BookerMapper;
import com.edreamsodigeo.accommodation.giata.model.Booker;
import com.edreamsodigeo.accommodation.giata.repository.JpaRepository;
import com.edreamsodigeo.accommodation.giata.repository.BookerRepository;
import com.google.inject.Inject;

import java.util.List;

public class JpaBookerRepository extends JpaRepository<Booker> implements BookerRepository {

    private final BookerMapper bookerMapper;

    @Inject
    public JpaBookerRepository(final BookerMapper bookerMapper) {
        this.bookerMapper = bookerMapper;
    }

    @Override
    public void saveBookerDTOListInBatch(List<BookerDTO> bookerDTOList) {
        final List<Booker> bookerList = bookerMapper.mapBookerDTOListToBookerList(bookerDTOList);
        saveDataInBatch(bookerList);
    }

    @Override
    public String getDataImportedCheckNamingQuery() {
        return "Book.count";
    }
}
