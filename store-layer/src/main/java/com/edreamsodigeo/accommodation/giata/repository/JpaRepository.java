package com.edreamsodigeo.accommodation.giata.repository;

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public abstract class JpaRepository<T> {

    private static final Logger LOGGER = Logger.getLogger(JpaRepository.class);

    private EntityManager entityManager;

    private static final int BATCH_SIZE = 50;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public abstract String getDataImportedCheckNamingQuery();

    public boolean isDataAlreadyImported() {
        Query query = entityManager.createNamedQuery(getDataImportedCheckNamingQuery());
        long result = (long) query.getSingleResult();
        return result > 0L;
    }

    public void saveDataInBatch(List<T> dtoList) {
        for (int index = 0; index < dtoList.size(); index++) {
            if (index % BATCH_SIZE == 0) {
                LOGGER.info(index + "-th data committed ...");
                this.entityManager.flush();
                this.entityManager.clear();
            }
            this.entityManager.persist(dtoList.get(index));
        }
    }

}
