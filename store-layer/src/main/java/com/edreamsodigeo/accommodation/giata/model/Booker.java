package com.edreamsodigeo.accommodation.giata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "BOOKER")
@Access(AccessType.FIELD)
@Getter
@Setter
@NamedQueries({@NamedQuery(name = "Book.count", query = "select count(b) from Booker b")})
public class Booker {

    @Id
    @Column(name = "EXPEDIA_ID", nullable = false)
    private Long expediaId;

}
