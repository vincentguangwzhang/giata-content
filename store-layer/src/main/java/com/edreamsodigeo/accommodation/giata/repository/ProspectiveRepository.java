package com.edreamsodigeo.accommodation.giata.repository;

import com.edreamsodigeo.accommodation.giata.ProspectiveDTO;

import java.util.List;

public interface ProspectiveRepository {

    void saveProspectiveDTOListInBatch(final List<ProspectiveDTO> prospectiveDTOList);

    boolean isDataAlreadyImported();

}
