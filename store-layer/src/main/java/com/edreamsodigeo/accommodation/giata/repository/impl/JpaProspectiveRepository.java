package com.edreamsodigeo.accommodation.giata.repository.impl;

import com.edreamsodigeo.accommodation.giata.ProspectiveDTO;
import com.edreamsodigeo.accommodation.giata.mapper.ProspectiveMapper;
import com.edreamsodigeo.accommodation.giata.model.Prospective;
import com.edreamsodigeo.accommodation.giata.repository.JpaRepository;
import com.edreamsodigeo.accommodation.giata.repository.ProspectiveRepository;
import com.google.inject.Inject;

import java.util.List;

public class JpaProspectiveRepository extends JpaRepository<Prospective> implements ProspectiveRepository {

    private final ProspectiveMapper prospectiveMapper;

    @Inject
    public JpaProspectiveRepository(final ProspectiveMapper prospectiveMapper) {
        this.prospectiveMapper = prospectiveMapper;
    }

    @Override
    public void saveProspectiveDTOListInBatch(List<ProspectiveDTO> prospectiveDTOList) {
        List<Prospective> prospectiveList = prospectiveMapper.mapProspectiveDTOListToProspectiveList(prospectiveDTOList);
        saveDataInBatch(prospectiveList);
    }

    @Override
    public String getDataImportedCheckNamingQuery() {
        return "Prospective.count";
    }
}
