package com.edreamsodigeo.accommodation.giata.mapper;

import com.edreamsodigeo.accommodation.giata.GiataMulticodesDTO;
import com.edreamsodigeo.accommodation.giata.model.GiataMulticodes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GiataMulticodesMapper {

    private final Map<String, String> gdsNameToCode;

    public GiataMulticodesMapper() {
        gdsNameToCode = new HashMap<>();
        gdsNameToCode.put("booking.com", "BC");
        gdsNameToCode.put("hotelbeds", "HB");
        gdsNameToCode.put("expedia_rapid", "EX");
    }

    public GiataMulticodes mapGiataMulticodesDTOToGiataMulticodes(GiataMulticodesDTO giataMulticodesDTO) {
        GiataMulticodes giataMulticodes = new GiataMulticodes();
        giataMulticodes.setGiataId(giataMulticodesDTO.getGiataId());
        giataMulticodes.setGdsID(giataMulticodesDTO.getGdsID());
        giataMulticodes.setGdsCode(gdsNameToCode.get(giataMulticodesDTO.getGdsName()));
        return giataMulticodes;
    }

    public List<GiataMulticodes> mapGiataMulticodesDTOToGiataMulticodesInBatch(List<GiataMulticodesDTO> giataMulticodesDTOS) {
        return giataMulticodesDTOS.stream().map(this::mapGiataMulticodesDTOToGiataMulticodes).collect(Collectors.toList());
    }

    public String mapGdsCodeByGdsName(String gds) {
        return gdsNameToCode.get(gds);
    }

}
