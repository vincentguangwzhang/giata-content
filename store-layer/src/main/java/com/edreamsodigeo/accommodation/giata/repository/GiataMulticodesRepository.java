package com.edreamsodigeo.accommodation.giata.repository;

import com.edreamsodigeo.accommodation.giata.GiataMulticodesDTO;

import java.util.List;

public interface GiataMulticodesRepository {

    void createGiataMulticodesInBatch(final List<GiataMulticodesDTO> giataMulticodesDTOs);

    void deleteGiataMulticodesByGds(final String gds);

}
