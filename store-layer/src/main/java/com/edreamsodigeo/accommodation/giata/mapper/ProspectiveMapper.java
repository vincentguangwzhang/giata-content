package com.edreamsodigeo.accommodation.giata.mapper;

import com.edreamsodigeo.accommodation.giata.ProspectiveDTO;
import com.edreamsodigeo.accommodation.giata.model.Prospective;

import java.util.List;
import java.util.stream.Collectors;

public class ProspectiveMapper {

    public Prospective mapProspectiveDTOToProspective(ProspectiveDTO prospectiveDTO) {
        Prospective prospective = new Prospective();
        prospective.setDedupId(prospectiveDTO.getDedupId());
        prospective.setSearchesPerDay(prospectiveDTO.getSearchesPerDay());
        return prospective;
    }

    public List<Prospective> mapProspectiveDTOListToProspectiveList(List<ProspectiveDTO> prospectiveDTOList) {
        return prospectiveDTOList.stream().map(this::mapProspectiveDTOToProspective).collect(Collectors.toList());
    }

}
