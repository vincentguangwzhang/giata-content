package com.edreamsodigeo.accommodation.giata.repository;

import com.edreamsodigeo.accommodation.giata.BookerDTO;

import java.util.List;

public interface BookerRepository {

    void saveBookerDTOListInBatch(final List<BookerDTO> bookerDTOList);

    boolean isDataAlreadyImported();

}
