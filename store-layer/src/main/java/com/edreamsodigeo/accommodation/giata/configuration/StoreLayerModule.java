package com.edreamsodigeo.accommodation.giata.configuration;

import com.edreamsodigeo.accommodation.giata.repository.BookerRepository;
import com.edreamsodigeo.accommodation.giata.repository.GiataMulticodesRepository;
import com.edreamsodigeo.accommodation.giata.repository.ProspectiveRepository;
import com.edreamsodigeo.accommodation.giata.repository.impl.JpaBookerRepository;
import com.edreamsodigeo.accommodation.giata.repository.impl.JpaGiataMulticodesRepository;
import com.edreamsodigeo.accommodation.giata.repository.impl.JpaProspectiveRepository;
import com.google.inject.AbstractModule;

/**
 * This class configures bindings for dependency injection.
 */
public class StoreLayerModule extends AbstractModule {

    @Override
    public void configure() {
        configureResourceLocator();
    }

    private void configureResourceLocator() {
        bind(GiataMulticodesRepository.class).to(JpaGiataMulticodesRepository.class);
        bind(BookerRepository.class).to(JpaBookerRepository.class);
        bind(ProspectiveRepository.class).to(JpaProspectiveRepository.class);
    }

}
