package com.edreamsodigeo.accommodation.giata.repository.impl;

import com.edreamsodigeo.accommodation.giata.GiataMulticodesDTO;
import com.edreamsodigeo.accommodation.giata.mapper.GiataMulticodesMapper;
import com.edreamsodigeo.accommodation.giata.model.GiataMulticodes;
import com.edreamsodigeo.accommodation.giata.repository.JpaRepository;
import com.edreamsodigeo.accommodation.giata.repository.GiataMulticodesRepository;
import com.google.inject.Inject;

import javax.persistence.Query;
import java.util.List;

public class JpaGiataMulticodesRepository extends JpaRepository<GiataMulticodes> implements GiataMulticodesRepository {

    private final GiataMulticodesMapper giataMulticodesMapper;

    @Inject
    public JpaGiataMulticodesRepository(GiataMulticodesMapper giataMulticodesMapper) {
        this.giataMulticodesMapper = giataMulticodesMapper;
    }

    @Override
    public void createGiataMulticodesInBatch(final List<GiataMulticodesDTO> giataMulticodesDTOs) {
        final List<GiataMulticodes> giataMulticodes = giataMulticodesMapper.mapGiataMulticodesDTOToGiataMulticodesInBatch(giataMulticodesDTOs);
        saveDataInBatch(giataMulticodes);
    }

    @Override
    public void deleteGiataMulticodesByGds(String gds) {
        String gdsCode = giataMulticodesMapper.mapGdsCodeByGdsName(gds);

        Query query = getEntityManager().createNamedQuery("GiataMulticodes.deleteByGdsCode");
        query.setParameter("gdsCode", gdsCode).executeUpdate();
    }

    @Override
    public String getDataImportedCheckNamingQuery() {
        return "GiataMulticodes.count";
    }
}
