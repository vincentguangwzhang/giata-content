package com.edreamsodigeo.accommodation.giata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "GIATA_DEDUPLICATION")
@Access(AccessType.FIELD)
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = "GiataMulticodes.count", query = "select count(g) from GiataMulticodes g"),
        @NamedQuery(name = "GiataMulticodes.deleteByGdsCode", query = "delete from GiataMulticodes g where g.gdsCode = :gdsCode"),
    })
public class GiataMulticodes implements Serializable {

    @Id
    @Column(name = "GIATA_ID", nullable = false)
    private Long giataId;

    @Id
    @Column(name = "GDS_CODE", nullable = false)
    private String gdsCode;

    @Id
    @Column(name = "GDS_ID", nullable = false)
    private String gdsID;

}
