package com.edreamsodigeo.accommodation.giata.mapper;

import com.edreamsodigeo.accommodation.giata.BookerDTO;
import com.edreamsodigeo.accommodation.giata.model.Booker;

import java.util.List;
import java.util.stream.Collectors;

public class BookerMapper {

    public Booker mapBookerDTOToBooker(BookerDTO bookerDTO) {
        Booker booker = new Booker();
        booker.setExpediaId(bookerDTO.getExpediaId());
        return booker;
    }

    public List<Booker> mapBookerDTOListToBookerList(List<BookerDTO> bookerDTOList) {
        return bookerDTOList.stream().map(this::mapBookerDTOToBooker).collect(Collectors.toList());
    }

}
