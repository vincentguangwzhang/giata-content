## Giata Content ##
[![Build Status](http://bcn-jenkins-01.odigeo.org/jenkins/buildStatus/icon?job=giata-content)](http://bcn-jenkins-01.odigeo.org/jenkins/job/giata-content/)
[![Coverage](http://qualitysonar.odigeo.org:8090/api/project_badges/measure?project=com.edreamsodigeo.accommodation%3Agiata-content-parent&metric=coverage)](http://qualitysonar.odigeo.org:8090/dashboard?id=com.edreamsodigeo.accommodation%3Agiata-content-parent)

### Introduction
This is a module maintained by [The room blenders](https://jira.edreamsodigeo.com/wiki/pages/viewpage.action?spaceKey=KB&title=The+Room+Blenders).

The Giata Content module is aimed build benchmark for comparing existing GDS provider hotel id and 

The goals of this module are the following:
* Load data from GIATA data server
* Load data from local EDO for bookers and prospective

## How to use it
* If you want to import GIATA GDS data, you can run as below. (Please to remind that you gds only support "booking.com", "hotelbeds", "expedia_rapid")
```
curl --location --request GET 'localhost:8080/giata-content/giata/gds/{gds}'
```

* If you want to import bookers and prospective data, you can run as below. Please to remind that you have to clean the ``booker`` and ``perspective`` table data first
```
curl --location --request POST 'http://localhost:8080/giata-content/files/import'
```

## Notice when you use
* It would take a very long time if you want to load gds data, please assume that at least 3 hours.
* It would take at least 5 minutes if you want to load booker and prospective data
* When import booker and perspective data, actually there is two csv files located in /service-layer/src/main/resources/com/edreamsodigeo/accommodation/giata/service
* If you update the book.csv file, please notice that all the booker id inside should be unique.

### Healthcheck environments
* [Prod](http://lb.hotel-content-services.gke-apps.edo.sv/hotel-content-services/engineering/healthcheck)

* [Integration](http://lb.hotel-content-services.gke-apps.edo.sv/hotel-content-services/engineering/healthcheck)

### Monitoring channels
* [AppDynamics](https://edreamsodigeo-production.saas.appdynamics.com/controller/#/location=APP_DASHBOARD&application=53&dashboardMode=force)

* [Grafana](https://grafana.services.odigeo.com/d/JZtjuEPmk/hotel-content-services?refresh=5m&orgId=13)

* [Kibana](https://kibana.logs.gke01-2.edo.sv/goto/32b4e94835862598dcf77c8aa6f69968)

### Technical information
* Docker
* Java 11
* maven 3
* Oracle

### Deploying the module in docker (local environment) ##

* Build the image with the next command `mvn clean install -P build-container-image`
* You can use docker-compose.yml to run the application locally:
    * To configure the secrets and create generated-docker-compose.yml `make -f generate-docker-compose-file`
    * To run the application: `docker-compose -f generated-docker-compose.yml up`
    * To stop the application: `docker-compose down`
    * More information in <https://jira.odigeo.com/wiki/display/ARCH/Dockerize+a+module#Dockerizeamodule-Runningmodules>
* The applications runs in: `http://localhost:8080/giata-content/engineering/ping`
* Logs will be saved in `~/containersLogs/giata-content`

NOTE: If you want to run the application with the QA database you need to change the connectionUrl of DataSource.properties.

### Launching functional tests in Docker ##
* On MacOS add the following to your ``/private/etc/hosts`` : ``127.0.0.1 host.docker.internal``
#### Launching all the tests
* Select these Profiles:
    * build-container-image
    * dev
    * get-local-ip (only in Windows machines)
    * mac-os (only in MacOs machines)
    * run-automated-test
* Execute clean + install from giata-content-parent (root)
* The report of the test execution will be in: \test\functional-tests\target\cucumber

#### Launching one single feature:
* Configuration:
    * Remove build in Before launch section
    * Add the next maven goal to launch section in functional-test working directory:
        * Working directory: [branch directory] + test
        * Command line: `clean install -P run-automated-test -Dmaven.test.skip.exec=true`

###  Remote configuration (only if needed)
* Add configuration
* Select *Remote* configuration
    * Host: localhost
    * Port: 8787
